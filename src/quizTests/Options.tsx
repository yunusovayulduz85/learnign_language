import React, { useContext } from 'react'
import "../index.css"
import { useQuiz } from '../context/QuizContext';
function Options({exactlyQuestion}:any) {
    const {dispatch,answer}=useQuiz();
    const hasAnswer = answer != null;
    return (
        <div className='options'>
            {
                exactlyQuestion?.options.map((option:string, index:number) =>
                (<button
                    className={`btn btn-option ${answer == index ? "answer" : ""} ${hasAnswer ? index == exactlyQuestion.correctOption ? "correct" : "wrong":""}`}
                    key={index} 
                    onClick={() => dispatch({ type: "newAnswer", payload: index })}>{option}</button>))
            }
        </div>
    )
}

export default Options


