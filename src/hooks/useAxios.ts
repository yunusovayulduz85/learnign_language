import axios from "axios";
import { useEffect, useState } from "react";
axios.defaults.baseURL = "http://localhost:9000";
const useAxios = (url:string) => {
    const [res, setRes] = useState<any>([]);
    const [error, setError] = useState<string>("");
    const [loader, setLoader] = useState<boolean>(true);
    useEffect(() => {
        const fetchData = async () => {
      try {
        const res = await axios.get(url);
        setRes(res.data);
        setLoader(false);
      } catch (err) {
        setError("Error fetching data");
      } finally{
        setLoader(false)
      }
    };

    fetchData();
    }, [])
    return { res, error, loader }
}

export default useAxios