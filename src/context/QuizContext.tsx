// import { createContext, useContext, useEffect, useReducer } from "react"
// type quizTestType = {
    
// }
// type actionType = {
//     type:string,
//     payload:number
// }
// type questionsType = {
//     correctOption:number,
//     points:number
// }[]
// interface InitialState  {
//     questions:[],
//     status:string,
//     index:number,
//     point:number,
//     answer: string | null,
//     secondRemaining:number
// }
// const initialState:InitialState = {
//     questions:[],
//     status:"loading",
//     index:0,
//     point:0,
//     answer:null,
//     secondRemaining:0
// }
// export const  quizContext = createContext<quizTestType>({
//     initialState
// });
// const SEC_PER1_QUESTIONS = 1;
// function reducer(state:InitialState, action:actionType) {
//     switch (action.type) {
//         case "dataReceive":
//             return {
//                 ...state,
//                 questions: action.payload,
//                 status: "ready"
//             }
//         case "error":
//             return {
//                 ...state,
//                 questions: action.payload,
//                 status: "error"
//             }
//         case "start":
//             return {
//                 ...state,
//                 status: "active",
//                 secondRemaining: state.questions.length * SEC_PER1_QUESTIONS
//             }
//         case "newAnswer":
//             const question:questionsType = state.questions[state.index];
//             return {
//                 ...state,
//                 answer: action.payload,
//                 point: action.payload === question.correctOption ? state.point + question.points : (state.point),
//             }
//         case "nextQuestions":
//             return {
//                 ...state,
//                 index: state.index + 1,
//                 answer: null
//             }
//         case "Restart":
//             return {
//                 ...initialState,
//                 questions: state.questions,
//                 status: "ready"
//             }
//         case "finished":
//             return {
//                 ...state,
//                 status: "finish"
//             }
//         case "tick":
//             return {
//                 ...state,
//                 secondRemaining: state.secondRemaining - 1,
//                 status: state.secondRemaining == 0 ? "finish" : state.status
//             }
//         default: throw new Error("This action is undefined")
//     }
// }

// const QuizContextProvider = ({ children,url }:any) => {
//     const [{ questions, status, index, point, answer, secondRemaining }, dispatch] = useReducer(reducer, initialState)
//     // console.log(status);
//     const numberOfQuestions = questions.length;
//     let maxPossiblePoints = questions.reduce((prev, cur) => prev + cur.points, 0);
   
//     useEffect(() => {
//         fetch("http://localhost:8080/questions"+url)
//             .then((res) => res.json())
//             .then((data) => dispatch({ type: "dataReceive", payload: data }))
//             .catch((err) => dispatch({ type: "error", payload: err.massage }))
//     }, [])
//     return (
//         <quizContext.Provider value={{numberOfQuestions,maxPossiblePoints,questions,index,dispatch,answer,secondRemaining,point,status}}>
//             {children}
//         </quizContext.Provider>
//     )
// }
//*
import React, { createContext, useEffect, useReducer, useContext, ReactNode } from 'react';
interface Question {
    correctOption:number,
    points:number,
    question:string
}

interface QuizState {
  questions: Question[];
  status: "loading" | "ready" | "error" | "active" | "next" | "finish";
  index: number;
  point: number;
  answer: number | null;
  secondRemaining: number;
  numberOfQuestions:number;
  maxPossiblePoints:number;
  dispatch: React.Dispatch<QuizAction>;
}

type QuizAction =
  | { type: "dataReceive"; payload: Question[] }
  | { type: "error"; payload: string }
  | { type: "start" }
  | { type: "newAnswer"; payload: number }
  | { type: "nextQuestions" }
  | { type: "Restart" }
  | { type: "finished" }
  | { type: "tick" };

const SEC_PER1_QUESTIONS = 1;

const initialState: QuizState = {
  questions: [],
  status: "loading",
  index: 0,
  point: 0,
  answer: null,
  secondRemaining: 0,
  numberOfQuestions:0,
  maxPossiblePoints:0,
  dispatch : () =>{}
};
const quizContext = createContext<QuizState>(initialState)

function reducer(state: QuizState, action: QuizAction): QuizState {
  switch (action.type) {
    case "dataReceive":
      return {
        ...state,
        questions: action.payload,
        status: "ready",
      };
    case "error":
      return {
        ...state,
        status: "error",
      };
    case "start":
      return {
        ...state,
        status: "active",
        secondRemaining: state.questions.length * SEC_PER1_QUESTIONS,
      };
    case "newAnswer":
      const question = state.questions[state.index];
      return {
        ...state,
        answer: action.payload,
        point: action.payload === question.correctOption ? state.point + question.points : state.point,
      };
    case "nextQuestions":
      return {
        ...state,
        index: state.index + 1,
        answer: null,
      };
    case "Restart":
      return {
        ...initialState,
        questions: state.questions,
        status: "ready",
      };
    case "finished":
      return {
        ...state,
        status: "finish",
      };
    case "tick":
      return {
        ...state,
        secondRemaining: state.secondRemaining - 1,
        status: state.secondRemaining === 0 ? "finish" : state.status,
      };
    default:
      throw new Error("This action is undefined");
  }
}

interface QuizContextProviderProps {
  children: ReactNode;
}

const QuizContextProvider: React.FC<QuizContextProviderProps> = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { questions, status, index, point, answer, secondRemaining,numberOfQuestions } = state;
  // numberOfQuestions = questions.length
  //  numberOfQuestions = questions.length;
  const maxPossiblePoints = questions.reduce((prev, cur) => prev + cur.points, 0);

  useEffect(() => {
    fetch("http://localhost:8080/english/")
      .then((res) =>res.json())           
      .then((data: Question[]) => dispatch({ type: "dataReceive", payload: data }))
      .catch((err) => dispatch({ type: "error", payload: err.message }));
  }, []);

  return (
    <quizContext.Provider value={{numberOfQuestions,maxPossiblePoints, questions, index, dispatch, answer, secondRemaining, point, status }}>
      {children}
    </quizContext.Provider>
  );
};

export function useQuiz() {
  return useContext(quizContext);
}

export default QuizContextProvider;