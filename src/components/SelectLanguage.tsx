import { Alert, AlertProps, Box, Button, MenuItem, Snackbar, TextField, Typography } from "@mui/material"
import { forwardRef, useState } from "react"
import { useNavigate } from "react-router"
const SnackBarAlert = forwardRef<HTMLDivElement,AlertProps>(
  function SnackBarAlert(props,ref) {
    return <Alert elevation={6} ref={ref} {...props}/>
  }
)

const SelectLanguage = () => {
  const [snackBarOpen,setSnackBarOpen] = useState(true)
  const [language, setLanguage] = useState("")
  const navigation = useNavigate()
  localStorage.setItem("language",language)
  if(language === "Eng") {
     localStorage.setItem("english",JSON.stringify([
        {
            "id":1,
            "lessonName":"Thema : Alphabet",
            "description":"Description : The English alphabet consist of 26 letters.There are 6 vowel letters and 21 consonant",
            "level":"Beginner",
            "data":"The English alphabet consists of 26 letters, which are divided into two categories: vowels and consonants. Here is the English alphabet:A B C D E F G H I J K L M N O P Q R S T U V W X Y Z ; Vowels: A,E,I,O,U;Consonants:B,C,D,F,G,H,J,K,L,M,N,O,P,Q,R,S,T,V,W,X,Y,Z;These letters are used to form words and sentences in the English language. The English alphabet follows the Latin script and is read from left to right. The order of the letters is standardized, and it serves as the basis for spelling and communication in English.",
            "lang":"eng"
        },
        {
            "id":2,
            "lessonName":"Thema : To be",
            "description":"Description : The verb to be is an auxiliary verb in English",
            "level":"Beginner",
            "data":"The verb 'to be' is an essential and irregular verb in English, used to indicate existence, identity, and various states. Here are its forms in different tenses:Present Tense:I am , You are, He/She/It is, We are , They are, Past Tense:I was,You were ,He/She/It was,We were,They were,Future Simple Tense:I will be, You will be, He/She/It will be, We will be,They will be , Present Continuous,Tense:I am being, You are being , He/She/It is being , We are being ,They are being , Past Continuous Tense:I was being , You were being , He/She/It was being, We were being ,They were being , Present Perfect Tense:I have been, You have been, He/She/It has been, We have been, They have been,Past Perfect Tense:I had been,You had been,He/She/It had been,We had been,They had been,Future Perfect Tense:I will have been,You will have been,He/She/It will have been,We will have been,They will have been,Present Perfect Continuous Tense:I have been being,You have been being,He/She/It has been being,We have been being,They have been being,Past Perfect Continuous Tense:I had been being,You had been being,He/She/It had been being,We had been being,They had been being,These forms cover a wide range of situations and time frames, making the verb 'to be' a versatile and important element in English grammar.",
            "lang":"eng"
        },
        {
            "id":3,
            "lessonName":"Thema : Present Continuous",
            "description":"Description : This tense is used to describe actions that are happening right now or around the current time.",
            "level":"Elementary",
            "data":"Affirmative (Positive) Form:I was studying when you called.You were working on the project yesterday.He/She/It was reading a book at that time.We were watching a movie when it started raining.They were playing in the garden when I saw them.Negative Form:I was not sleeping when the phone rangYou were not attending the meeting at 3 PM.He/She/It was not watching TV when I arrived.We were not traveling last weekend.They were not talking about you.Question Form:Was I interrupting you?Were you enjoying the party?Was he/she/it working on a project?Were we going to the cinema last night?Were they playing basketball?In the past continuous tense, the verb 'to be' is conjugated in the past tense, followed by the past participle (the -ing form) of the main verb. This tense is used to describe actions that were in progress at a specific point in the past or were happening simultaneously.",
            "lang":"eng"
        },
        {
            "id":4,
            "lessonName":"Thema : Present Simple",
            "description":"Description :  It is used to describe general truths, habitual actions, or current states.",
            "level":"Level : Elementary",
            "data":"Affirmative (Positive) Form:I am a student.You are my friend.He/She/It is a doctor.We are colleagues.They are musicians.Negative Form:I am not a teacher.You are not late.He/She/It is not hungry.We are not coming to the party.They are not on vacation.Question Form:Am I your roommate?Are you attending the meeting?Is he/she/it the manager?Are we ready to leave?Are they siblings?In the present simple tense, the verb 'to be' is conjugated according to the subject. The negative form is often created by adding 'not' after the verb 'to be', and questions are formed by inverting the subject and the verb. This tense is used to express general truths, habitual actions, or current states of being.",
            "lang":"eng"
        },
        {
            "id":5,
            "lessonName":"Thema : Past Simple",
            "description":"Description : In the past simple tense, the verb 'to be' is conjugated according to the subject and the time reference is usually in the past.",
            "level":"Level : Elementary",
            "data":"Affirmative (Positive) Form:I was at the store yesterday.You were at the party last night.He/She/It was on vacation last month.We were in the park this morning.They were at the concert yesterday.Negative Form:I was not at work yesterday.You were not at the meeting.He/She/It was not happy with the result.We were not in class last week.They were not aware of the changes.Question Form:Was I late for the train?Were you at home yesterday?Was he/she/it at the event?Were we invited to the party?Were they surprised by the news?In the past simple tense, the verb 'to be' is conjugated according to the subject and the time reference is usually in the past. The negative form is often created by adding 'not' after the verb 'to be', and questions are formed by inverting the subject and the verb. This tense is used to describe completed actions or states in the past.",
            "lang":"eng"
        },
        {
            "id":6,
            "lessonName":"Thema : Past Continuous",
            "description":"Description : This tense is used to describe actions that were in progress at a specific point in the past or were happening simultaneously.",
            "level":"Level : Elementary",
            "data":"Affirmative (Positive) Form:I was studying when you called.You were working on the project yesterday.He/She/It was reading a book at that time.We were watching a movie when it started raining.They were playing in the garden when I saw them.Negative Form:I was not sleeping when the phone rang.You were not attending the meeting at 3 PM.He/She/It was not watching TV when I arrived.We were not traveling last weekend.They were not talking about you.Question Form:Was I interrupting you?Were you enjoying the party?Was he/she/it working on a project?Were we going to the cinema last night?Were they playing basketball?In the past continuous tense, the verb 'to be' is conjugated in the past tense, followed by the past participle (the -ing form) of the main verb. This tense is used to describe actions that were in progress at a specific point in the past or were happening simultaneously.",
            "lang":"eng"
        },
        {
            "id":7,
            "lessonName":"Thema : Present Perfect",
            "description":"Description : This tense is often used to express actions that occurred at an indefinite time in the past but have relevance to the present moment.",
            "level":"Level : Pre-intermediate",
            "data":"Affirmative (Positive) Form:I have been to Paris.You have been successful in your endeavors.He/She/It has been a great leader.We have been friends for a long time.They have been working hard.Negative Form:I have not been to Asia yet.You have not been absent from class.He/She/It has not been to the new office.We have not been on vacation this year.They have not been involved in the project.Question Form:Have I been helpful?Have you been to the museum?Has he/she/it been informed?Have we been working on the same project?Have they been participating actively?In the present perfect tense, the verb 'to be' is conjugated in the present perfect form, followed by the past participle of the main verb. This tense is often used to express actions that occurred at an indefinite time in the past but have relevance to the present moment.",
            "lang":"eng"
        },
        {
            "id":8,
            "lessonName":"Thema : Passive voice",
            "description":"Description : The passive voice is often used to shift the focus from the doer of the action to the action itself.",
            "level":"Level : Pre-intermediate",
            "data":"The passive voice is a grammatical construction in which the subject of a sentence is the receiver of the action, rather than the doer. In passive voice sentences, the emphasis is on the action itself, and the agent (the one performing the action) may or may not be mentioned.The passive voice is formed using a combination of the appropriate form of the verb 'to be' and the past participle of the main verb. Here are examples using the verb 'to be' in the passive voice:Present Simple Passive:The report is written by the team.The cake is baked by Mary every Sunday.The letters are typed by the secretary.The room is cleaned by the janitor.The books are read by the students.Past Simple Passive:The movie was watched by many people.The message was delivered by the courier.The song was sung by the choir.The novel was written by the author.The project was completed last week.Present Perfect Passive:The job has been done by the team.The document has been reviewed by the manager.The repairs have been finished by the technicians.The information has been updated regularly.The questions have been answered by the teacher.Future Simple Passive:The new software will be installed by the IT department.The winners will be announced next week.The event will be organized by the planning committee.The project will be completed by the end of the month.The report will be submitted by the team.Passive voice is often used when the doer of the action is unknown, not important, or when the focus is on the action itself rather than the person performing it.",
            "lang":"eng"
        },
        {
            "id":9,
            "lessonName":"Thema : Regular and Irregular verbs",
            "description":"Description : Understanding the difference between regular and irregular verbs is crucial for constructing sentences in various tenses correctly.",
            "level":"Level : Pre-intermediate",
            "data":"Regular Verbs:Formation of Past Tense:For regular verbs, the past tense is formed by adding '-ed' to the base form of the verb.Examples:Infinitive: walkPast Tense: walkedInfinitive: playPast Tense: playedIrregular Verbs:Formation of Past Tense:Irregular verbs do not follow a consistent pattern when forming their past tense. They undergo unique changes in their base form.Examples:Infinitive: goPast Tense: went Infinitive: sing Past Tense: sangInfinitive: eat Past Tense: ate Irregular verbs often need to be memorized because their past tense forms are not predictable. In addition to the past tense, irregular verbs also have irregular past participles.Examples:Infinitive: go,Past Participle: gone,Infinitive: sing,Past Participle: sung,Infinitive: eat,Past Participle: eaten,These examples illustrate the difference between regular and irregular verbs in terms of forming the past tense and past participle. Regular verbs follow a pattern, while irregular verbs have unique forms that need to be learned individually.",
            "lang":"eng"
        },
        {
            "id":10,
            "lessonName":"Thema : Future tenses",
            "description":"Description : Each future tense serves a specific purpose and is used based on the context and the speaker's intention. ",
            "level":"Level : Pre-intermediate",
            "data":"There are several ways to express future actions in English, and different future tenses are used to convey different nuances. Here are the main future tenses in English:1. Future Simple (Will):Affirmative:I will go to the store.She will finish her project.Negative:I will not attend the meeting.They will not travel this summer.Question:Will you join us for dinner?Will they arrive on time?2. Future Simple (Be Going To):Affirmative:I am going to visit my grandparents.We are going to start a new business.Negative:She is not going to buy a new car.They are not going to move to another city.Question:Are you going to attend the conference?Are they going to celebrate the anniversary?3. Future Continuous:Affirmative:I will be studying at that time.He will be working on the project.Negative:We will not be traveling next week.She will not be attending the event.Question:Will you be waiting for me?Will they be participating in the competition?4. Future Perfect:Affirmative:By next year, I will have completed my degree.She will have finished reading the book by tomorrow.Negative:They will not have arrived by the time we leave.I will not have received the package by Friday.Question:Will you have submitted your report by then?Will she have reached the destination?5. Future Perfect Continuous:Affirmative:By next month, I will have been working here for five years.They will have been living in this city for a decade.Negative:She will not have been waiting for too long.We will not have been using this software for a year.Question:Will you have been working on the project for a month by then?Will they have been traveling for two weeks?Each future tense serves a specific purpose and is used based on the context and the speaker's intention. The choice between these tenses depends on whether the action will be completed, ongoing, or in progress at a certain point in the future.",
            "lang":"eng"
        }
    ]))
  }
  else if (language === "Rus"){
    localStorage.setItem("russian",JSON.stringify([
        {
            "id":1,
            "lessonName":"Тема : Алфавит",
            "description":"Описание : Русский алфавит состоит из 33 букв ",
            "level":"Уровень : A1",
            "data":"Русский алфавит состоит из 33 букв и включает в себя следующие символы:А Б В Г Д Е Ё Ж З И Й К Л М Н О П Р С Т У Ф Х Ц Ч Ш Щ Ъ Ы Ь Э Ю ЯНапомню, что в русском алфавите есть буквы, которые отсутствуют в английском алфавите, такие как Ё, Ж, Й, Ч, Ш, Щ, Ъ, Ы, Ь, Э, Ю, Я.",
            "lang":"rus"
        },
        {
            "id":2,
            "lessonName":"Тема : Притяжательные местоимения",
            "description":"Описание : Притяжательные местоимения в русском языке используются для выражения принадлежности или принадлежности к кому-то или чему-то.",
            "level":"Уровень : A1",
            "data":"Притяжательные местоимения в русском языке используются для выражения принадлежности или принадлежности к кому-то или чему-то. Вот притяжательные местоимения в русском языке:Мой, моя, моё, моиМой дом (my house)Моя машина (my car)Моё окно (my window)Мои друзья (my friends)Твой, твоя, твоё, твоиТвой рюкзак (your backpack)Твоя книга (your book)Твоё устройство (your device)Твои заботы (your concerns)Его, её, его, ихЕго кот (his cat)Её дом (her house)Его решение (his decision)Их проект (their project)Наш, наша, наше, нашиНаш город (our city)Наши дети (our children)Наша идея (our idea)Наши друзья (our friends)Ваш, ваша, ваше, ваши Ваш билет (your ticket)Ваша комната (your room)Ваше мнение (your opinion)Ваши планы (your plans)ИхИх семья (their family)Их книги (their books)Их учитель (their teacher)Эти притяжательные местоимения меняют свою форму в зависимости от рода, числа и падежа существительного, к которому они относятся.",
             "lang":"rus"
        },
        {
            "id":3,
            "lessonName":"Тема : Множесвенное число",
            "description":"Описание : В русском языке существует несколько правил для образования множественного числа существительных",
            "level":"Уровень : А1",
            "data":"В русском языке существует несколько правил для образования множественного числа существительных. Вот основные правила:1**Существительные с основой на согласную:**- Если существительное заканчивается на согласную, меняется окончание:- стол - столы,- дом - дома,- кот - коты2. **Существительные с основой на мягкий знакь (ь) или й:**- Если существительное оканчивается на мягкий знак (ь) или й, добавляется -и:- лужайка - лужайки,- ключ - ключи3. **Существительные с основой на -а или -я:**- Если существительное заканчивается на -а или -я, заменяется на -и:- книга - книги- река - реки4. **Существительные с основой на -о или -е:**- Если существительное заканчивается на -о или -е, меняется на -а:- окно - окна- зеркало - зеркала5. **Существительные с основой на -ий, -ие, -ье:**- Заменяются на -ия:- гражданин - граждане,- пение - пения,- личное - личности6. **Несклоняемые существительные:**- Некоторые существительные остаются неизменными во множественном числе:- молоко - молоко,- мебель - мебель,- золото - золото Это основные правила для образования множественного числа в русском языке, но также есть исключения и неправильные склонения, которые нужно запомнить.",
            "lang":"rus"
        },
        {
            "id":4,
            "lessonName":"Тема : Имя прилагателно",
            "description":"Описание : В русском языке существует три степени сравнения прилагательных",
            "level":"Уровень : А1",
            "data":"В русском языке существует три степени сравнения прилагательных: положительная, сравнительная и превосходная. Примеры для прилагательного 'красивый':1. **Положительная степень (Какой?):**- красивый (beautiful)2. **Сравнительная степень (Как?):**- красивее (more beautiful)- красивей (more beautiful) - употребляется в неформальной разговорной речи3. **Превосходная степень (Какой из всех?):**- самый красивый (the most beautiful)Примеры для прилагательного 'интересный':1. **Положительная степень (Какой?):**- интересный (interesting)2. **Сравнительная степень (Как?):**- интереснее (more interesting)- интересней (more interesting) - употребляется в неформальной разговорной речи3. **Превосходная степень (Какой из всех?):**- самый интересный (the most interesting)Примеры для прилагательного 'высокий':1. **Положительная степень (Какой?):**- высокий (high, tall)2. **Сравнительная степень (Как?):**- выше (higher),- выше́й (higher) - употребляется в неформальной разговорной речи3. **Превосходная степень (Какой из всех?):**- самый высокий (the highest)Это основные формы прилагательных в различных степенях сравнения. В русском языке есть несколько правил образования сравнительной и превосходной степеней, но также существуют некоторые исключения и неправильные формы.",
            "lang":"rus"
        },
        {
            "id":5,
            "lessonName":"Тема : Имя Числително",
            "description":"Описание : Имя числительное — это часть речи, которая обозначает количество или порядок предметов.",
            "level":"Уровень : А1",
            "data":"Имя числительное — это часть речи, которая обозначает количество или порядок предметов. В русском языке существуют кардинальные (обозначают количество) и порядковые (обозначают порядок) числительные.### Кардинальные числительные (обозначают количество):1. **Один** (1)2. **Два** (2)3.**Три** (3)4. **Четыре** (4)5. **Пять** (5)6. **Шесть** (6)7. **Семь** (7)8. **Восемь** (8)9. **Девять** (9)10. **Десять** (10)11. **Сто** (100)12. **Тысяча** (1000)13. **Миллион** (1,000,000)14. **Миллиард** (1,000,000,000)15. И так далее.Примеры:- У меня есть **три** яблока.- Он прочитал **десять** книг.### Порядковые числительные (обозначают порядок):1. **Первый** (1-й)2. **Второй** (2-й)3. **Третий** (3-й)4. **Четвёртый** (4-й)5. **Пятый** (5-й)6. **Шестой** (6-й)7. **Седьмой** (7-й)8. **Восьмой** (8-й)9. **Девятый** (9-й)10. **Десятый** (10-й)11. **Сотый** (100-й)12. **Тысячный** (1000-й)13. **Миллионный** (1,000,000-й)14. **Миллиардный** (1,000,000,000-й)15. И так далее.Примеры:- Это мой **первый** опыт работы.- Он занял **второе** место в соревнованиях.Эти числительные используются для обозначения порядка, ранжирования или расположения предметов или событий.",
            "lang":"rus"
        },
        {
            "id":6,
            "lessonName":"Тема : Прошедше время",
            "description":"Описание : Для обозначения прошедшего времени в русском языке используются различные временные формы глаголов.",
            "level":"Уровень : А1",
            "data":"Для обозначения прошедшего времени в русском языке используются различные временные формы глаголов. Вот основные формы для прошедшего времени:### Прошедшее несовершенное время:Прошедшее несовершенное время обозначает действие, которое происходило в прошлом и продолжалось в течение некоторого времени.- **Я читал(а)** (I was reading)- **Ты писал(а)** (You were writing)- **Он/она гулял(а)** (He/She was walking)- **Мы слушали** (We were listening)- **Вы смотрели** (You were watching)- **Они играли** (They were playing)### Прошедшее совершенное время:Прошедшее совершенное время используется для обозначения завершенных действий в прошлом.- **Я прочитал(а)** (I have read)- **Ты написал(а)** (You wrote)- **Он/она пошёл(а)** (He/She went)- **Мы услышали** (We heard)- **Вы посмотрели** (You watched)- **Они сыграли** (They played)### Прошедшее простое время:Прошедшее простое время обозначает действия, которые произошли в определенный момент в прошлом.- **Я читал(а)** (I read)- **Ты писал(а)** (You wrote- **Он/она гулял(а)** (He/She walked) - **Мы слушали** (We listened) - **Вы смотрели** (You watched) - **Они играли** (They played)### Прошедшее длительное время:Прошедшее длительное время используется для выражения продолжительности действия в прошлом.- **Я работал(а)** (I worked) - **Ты учи(а)ся** (You studied) - **Он/она ждал(а)** (He/She waited)- **Мы путешествовали** (We traveled)- **Вы готовили** (You cooked)- **Они спали** (They slept)Выбор временной формы зависит от контекста и характера действия в прошлом.",
            "lang":"rus"
        },
        {
            "id":7,
            "lessonName":"Тема : Будущее время",
            "description":"Описание : Для выражения будущего времени в русском языке используются различные формы глаголов.",
            "level":"Уровень : А1",
            "data":"Для выражения будущего времени в русском языке используются различные формы глаголов. Вот основные формы для будущего времени:### Будущее несовершенное время:Будущее несовершенное время используется для выражения действия, которое будет продолжаться в будущем.- **Я буду читать** (I will be reading)- **Ты будешь писать** (You will be writing)- **Он/она будет гулять** (He/She willbe walking)- **Мы будем слушать** (We will be listening)- **Вы будете смотреть** (You will be watching)- **Они будут играть** (They will be playing)### Будущее совершенное время:Будущее совершенное время обозначает действие,которое будет завершено к определенному моменту в будущем.- **Я буду прочитав** (I will have read)- **Ты будешь написав** (You will have written)- **Он/она будет пошёл(а)** (He/She will have gone)- **Мы будем услышав** (We will have heard)- **Вы будете посмотрев** (You will have watched)- **Они будут сыграв** (They will have played)### Будущее простое время:Будущее простое время используется для выражения действий, которые произойдут в будущем в определенный момент.- **Я буду читать** (I will read)- **Ты будешь писать** (You will write)- **Он/она будет гулять** (He/She will walk)- **Мы будем слушать** (We will listen)- **Вы будете смотреть** (You will watch)- **Они будут играть** (They will play)### Будущее длительное время:Будущее длительное время используется для выражения продолжительности действия в будущем.- **Я буду работать** (I will be working)- **Ты будешь учиться** (You will be studying)- **Он/она будет ждать** (He/She will be waiting)- **Мы будем путешествовать** (We will be traveling)- **Вы будете готовить** (You will be cooking)- **Они будут спать** (They will be sleeping)Выбор временной формы зависит от контекста и характера действия в будущем.",
            "lang":"rus"
        },
        {
            "id":8,
            "lessonName":"Тема : Наречие",
            "description":"Описание : Наречие в русском языке — это часть речи, которая изменяет или уточняет значение глагола, прилагательного, наречия, предлога или целого предложения. ",
            "level":"Уровень : А1",
            "data":"Наречие в русском языке — это часть речи, которая изменяет или уточняет значение глагола, прилагательного, наречия, предлога или целого предложения. Наречия отвечают на вопросы 'как?','где?', 'когда?', 'почему?', 'в какой мере?' и т.д. Они добавляют дополнительную информацию к действиям или качествам.Примеры наречий:1. **Как?**- *Быстро* бежать.- *Тщательно* проверить работу.- *Громко* говорить.2. **Где?**- Он живет *здесь*.- Мы гуляли *в парке*.   - Книги лежат *в шкафу*.3. **Когда?**- Они придут *позже*.- Мы начнем *завтра*.- Я увижу тебя *вечером*.4. **Почему?**- Он ушел *потому что* злая.- Они смеялись *от радости*.- Я не пойду *из-за дождя*.5. **В какой мере?**- Это произошло *очень* быстро.- Он говорит *громко*.- Мы почувствовали *сильный* ветер.6. **Сколько?**- У него *много* друзей.- Мы выпили *немного* чая.- Она купила *дважды*.Наречия могут быть образованы от прилагательных или существительных, добавлением суффиксов. Например, от прилагательного 'хороший' образуется наречие 'хорошо', от существительного 'дом' — 'дома.",
            "lang":"rus"
        },
        {
            "id":9,
            "lessonName":"Тема : Повелителное наколение глагола",
            "description":"Описание : Повелительное наклонение в русском языке используется для выражения приказов, просьб или предложений. ",
            "level":"Уровень : А1",
            "data":"Повелительное наклонение в русском языке используется для выражения приказов, просьб или предложений. Оно формируется от основы глагола и зависит от лица и числа, к которому обращается говорящий.### Повелительное наклонение для ты (единственное число):1. **Глаголы на -ть:**- **Читай** (Read)- **Пиши** (Write)- **Иди** (Go)2. **Глаголы на -и:**- **Беги** (Run)- **Гуляй** (Walk)- **Смотри** (Look)### Повелительное наклонение для вы (единственное число):1. **Глаголы на -ть:**- **Читайте** (Read)- **Пишите** (Write)- **Идите** (Go)2. **Глаголы на -и:**- **Бегите** (Run)- **Гуляйте** (Walk)- **Смотрите** (Look)### Повелительное наклонение для мы:1. **Глаголы на -ть:**- **Читайте** (Read)- **Пишите** (Write)- **Идите** (Go)2. **Глаголы на -и:**- **Бегите** (Run)- **Гуляйте** (Walk)- **Смотрите** (Look)### Повелительное наклонение для вы (множественное число):1. **Глаголы на -ть:**- **Читайте** (Read)- **Пишите** (Write)- **Идите** (Go)2. **Глаголы на -и:**- **Бегите** (Run)- **Гуляйте** (Walk)- **Смотрите** (Look)Это основные формы повелительного наклонения для различных лиц и чисел. При образовании повелительного наклонения существуют некоторые особенности, такие как убирание окончания -ть у некоторых глаголов.",
            "lang":"rus"
        },
        {
            "id":10,
            "lessonName":"Тема : Началное форма глагола",
            "description":"Описание : Начальная форма глагола в русском языке называется 'инфинитив'",
            "level":"Уровень : А1",
            "data":"Начальная форма глагола в русском языке называется 'инфинитив'. Инфинитив — это неизменяемая форма глагола, которая не зависит от лица, числа или времени. В русском языке инфинитив обычно заканчивается на -ть, -чь, -сть, -зть, например:- **Читать** (to read),- **Писать** (to write),- **Говорить** (to speak),- **Бежать** (to run),- **Идти** (to go),- **Петь** (to sing),- **Есть** (to eat),- **Смотреть** (to watch),Инфинитив используется в различных грамматических конструкциях, в том числе после модальных глаголов (например, 'мочь', 'хотеть', 'должен'), после предлогов, в сложносочиненных и сложноподчиненных предложениях, а также в форме герундия.",
            "lang":"rus"
        }
       
    ]))
  }
  else if(language === "Uzb"){
     localStorage.setItem("uzbek",JSON.stringify([
        {
            "id":1,
            "lessonName":"Mavzu : O'zbek Alifbosi",
            "description":"Qisqacha : Bu alifbo oʻzbek tili uchun xususiy tuzilmaga ega boʻlib, uning harflari va belgilarini oʻz ichiga oladi.",
            "level":"Daraja : A1",
            "data":"Oʻzbek alifbosi — Oʻzbek tilidagi alifboni ifodalovchi tizimdir. Bu alifbo oʻzbek tili uchun xususiy tuzilmaga ega boʻlib, uning harflari va belgilarini oʻz ichiga oladi. Oʻzbek alifbosi 29 ta harf va undan tuzilgan 9 ta unli harf (vokal) bilan ajratiladi. Quyidagi Oʻzbek alifbosi:1. **A a**,2. **B b**,3. **D d**,4. **E e**,5. **F f**,6. **G g**,7. **Gʻ gʻ**,8. **H h**,9. **I i**,10. **J j**,11. **K k**,12. **L l**,13. **M m**,14. **N n**,15. **O o**,16. **P p**,17. **Q q**,18. **R r**,19. **S s**,20. **Sh sh**,21. **T t**,22. **U u**,23. **V v**,24. **X x**,25. **Y y**,26. **Z z**,27. **Oʻ oʻ**,28. **Gʻʻ gʻʻ**,29. **Ch ch**,Oʻzbek alifbosidagi harflar va belgilar oʻzbek tili matnlarini yozish uchun ishlatiladi. Har bir harfning oʻz tavsifi va uni ifodalovchi misollar, soʻzlar ham mavjud.",
            "lang":"uzb"
        },
        {
            "id":2,
            "lessonName":"Mavzu : Fonetika",
            "description":"Qisqacha : “Oʻzbek tili fonetika mazmuni” iborasi Oʻzbekistonda davlat tili boʻlgan oʻzbek tilining fonologik (fonetik) mazmunini bildiradi.",
            "level":"Daraja : A1",
            "data":"“Oʻzbek tili fonetika mazmuni” iborasi Oʻzbekistonda davlat tili boʻlgan oʻzbek tilining fonologik (fonetik) mazmunini bildiradi. Fonetika tildagi tovushlarni va bu tovushlar qanday hosil boʻlishini oʻrganuvchi tilshunoslikning boʻlimidir.Oʻzbek tili turkiy tillar oilasiga mansub boʻlib, Oʻzbekistonda rasmiy til sifatida qoʻllaniladi. O‘zbek tilida fonetik jihatdan xilma-xil tovushlar mavjud. Bu tovushlarni unlilar (vokal) va undoshlar (undoshlar) deb tasniflash mumkin.Keling, bir nechta asosiy oʻzbek tovushlarini namuna qilib olaylik:### Unlilar (vokal):1. **a:** [a] (masalan, “kitob”dagi “a”),2. **e:** [e] (masalan, “metbex”dagi “e),3. **i:** [i] (masalan, “tinch”dagi “i”),4. **o:** [o] (masalan, “domla”dagi “o”),5. **u:** [u] (masalan, “quruv”dagi “u”),### Undoshlar (undoshlar):1. **b:** [b] (masalan, “bola”dagi “b”),2. **d:** [d] (masalan, “dunyo”dagi “d”),3. **g:** [g] (masalan, “gul”dagi “g”),4. **j:** [ʤ] (masalan, “jamoat”dagi “j”),5. **q:** [q] (masalan, “qora”dagi “q”),Bu tovushlar o‘zbek tilidagi asosiy tovushlarning bir qismidir. To'liq fonetik tushuntirish uchun maxsus grammatika yoki fonologiya manbasiga murojaat qilish tavsiya etiladi.",
            "lang":"uzb"
        },
        {
            "id":3,
            "lessonName":"Mavzu : Leksikologiya",
            "description":"Qisqacha : Leksikologiya (leksika va …logiya) — tilshunoslikning til lugʻat tarkibi",
            "level":"Daraja : A1",
            "data":"Leksikologiya (leksika va …logiya) — tilshunoslikning til lugʻat tarkibi, yaʼni muayyan bir tilning leksikasini oʻrganuvchi boʻlimi. L. har bir soʻzni yolgʻiz holda emas, balki boshqa soʻzlar bilan bogʻliq holda oʻrganadi. L. tilshunoslikning leksikografiya, frazeologiya, semasiologiya yoki semantika, etimologiya, stilistika hamda soʻz yasalishi haqidagi taʼlimot kabi sohalari bilan chambarchas bogʻliq. L.ning asosiy muammolaridan biri soʻzning mustaqil til birligi sifatida mavjudligi masalasidir. L.da soʻzlarning maʼno jihatdan oʻzaro bogʻlangan, yaʼni monosemiya, polisemiya, sinonimiya, antonimiya, soʻz maʼnolarining erkin yoki bogʻliq holda boʻlishi kabi masalalari ham oʻrganiladi. Leksika muayyan bir tizim sifatida koʻrilganda, soʻz maʼnolari va tushunchalarning oʻzaro bogʻliq holda boʻlishi koʻzda tutiladi. L. lugʻat tarkibining amalda ishlatilishi va taraqqiyoti qonuniyatlarini, soʻzlarning uslubiy jihatdan tasnifiy tamoyillarini ishlab chiqadi. Shuningdek, soʻzlashuv va adabiy tillarda foydalanish meʼyorlarini, professionalizm, dialektizm, arxaizm, neologizm, leksikalashgan soʻz birikmalarini meʼyorlashtirish kabi masalalarni tahlil etadi hamda bular haqida muayyan xulosalar chiqaradi.Leksikologiya atama yunoncha 2 soʻzning jamlashuvi natijasida vujudga keladi: „soʻz yoki soʻz uchun“ va- „oʻrganish“. Boshqa maʼnoda „oʻrganish, mulohaza yuritish, tushuntirish, mavzu“ degan maʼnoda ham ishlatilishi mumkin. Etimologiya fan sifatida aslida leksikologiyaning diqqat markazidir. Leksikologiya soʻzlarning maʼnosini va ularning semantik munosabatlarini oʻrganganligi sababli, koʻpincha soʻzning tarixi va rivojlanishini oʻrganadi. Etimologlar qarindosh tillarni qiyosiy usul yordamida tahlil qiladilar, bu tilshunoslarga ajdodlarning fonologik, morfologik, sintaktik va hokazolarni tiklashga imkon beradigan texnikalar toʻplamidir, ularning qarindosh materiallarini taqqoslash orqali zamonaviy tillarning tarkibiy qismlari va kelib chiqishini oʻrganadi.",
            "lang":"uzb"
        },
        {
            "id":4,
            "lessonName":"Mavzu : Morfologiya",
            "description":"Qisqacha : Morfologiya. so'z turkumlari, ularga xos grammatik maʼnolarni o'rganadi",
            "level":"Daraja : A1",
            "data":"Morfologiya (yun. morphe — shakl va... logiya) (tilshunoslikda) — 1) tilning morfologik qurilishi; 2) soʻz shakllari haqidagi taʼlimot. Birinchi maʼnosida obyektni anglatsa, ikkinchi maʼnosida tilshunoslikning shu obyektni oʻrganuvchi bo'limini bildiradi.U shuningdek 3ta katta guruhga bo'linadi: 1) mustaqil so'z turkumlari; 2) yordamchi so'zlar; 3) oraliqdagi sozlar (alohida olingan so'zlar)Morfologiya. so'z turkumlari, ularga xos grammatik maʼnolarni, har bir turkumga xos grammatik kategoriyalar, bu kategoriyalarni yuzaga keltiruvchi grammatik shakl va grammatik maʼnolar va shahrikni oʻrganadi. Til tizimdan iborat boʻlganidek, uning M.si ham oʻziga xos tizimni tashkil etadi. Oʻz navbatida, morfologik tizim ham oʻziga xos kichik tizimlardan tashkil topadi. Ulardan har birining mohiyati yoritilishi bilan, tilning M.si yaxlit holda, tizim sifatida oʻrganiladi.Har bir soʻz turkumiga xos ichki tizim (tizimcha)larni shu turkumga xos morfologik kategoriyalar tashkil etadi. Morfologik kategoriyalar soʻz turkumiga xos maʼlum bir hodisaga oid umumiy va xususiy maʼnolar va bu maʼnolarni ifodalovchi soʻz shakllari birligidan iborat boʻladi. Ana shu soʻz shakllari va ularga xos umumiy va xususiy maʼnolar yoritilishi bilan muayyan morfologik kategoriyalarning mohiyati belgilanadi. Boshqacha aytganda, morfologik tizim ichidagi ichki tizimlardan birining mohiyati belgilanadi. Mas, feʼlning zamon kategoriyasi feʼl M.sida alohida tizimni tashkil etadi. Shuning uchun feʼlning zamonlariga nisbatan 'feʼl zamonlari tizimi' degan ibora ham qoʻllanadi. Feʼlning zamon kategoriyasi, zamon tizimining mohiyati shundan iboratki, zamon shakllarining barchasi harakatning nutq vaqtiga (nutq momentiga) munosabatini bildiradi. Bu — zamon shakllarining barchasi uchun umumiy boʻlgan xususiyat. Shu bilan birga har bir zamonga oid feʼl shakli (shakllari) oʻziga xos xususiyatga ega. Mas, oʻtgan zamon shakllari harakatning nutq vaqtigacha, hozirgi zamon shakllari harakatning nutq vaqtida, kelasi zamon shakllari harakatning nutq vaqtidan keyin bajarilishini bildiradi. Feʼl zamon shakllariga xos ana shu umumiy va xususiyliklar zamon kategoriyasining, feʼl zamonlari tizimining mohiyatidir. Demak, feʼl turkumiga oid har bir morfologik kategoriyaning mohiyatini aniqlash bilan feʼlning morfologik tizimi yoritiladi.[1]",
            "lang":"uzb"
        },
        {
            "id":5,
            "lessonName":"Mavzu : Yordamchi so'zlar",
            "description":"Qisqacha : Yordamchi soʻzlar - mustaqil gap boʻlagi vazifasida kela olmaydigan..",
            "level":"Daraja : A1",
            "data":"Yordamchi soʻzlar - mustaqil gap boʻlagi vazifasida kela olmaydigan, yordamchi maʼno va funksiyada qoʻllanadigan soʻzlar. Bular gapda mustaqil boʻlak boʻlib keluvchi soʻzlarni bogʻlaydi yoki ularga grammatik (sintaktik) tavsif beradi (mas, artikl). Oʻzbek tilida vazifasiga koʻra, yordamchi soʻzlar 3 turga boʻlinadi: koʻmakchi; bogʻlovchi; yuklama.",
            "lang":"uzb"
        },
        {
            "id":6,
            "lessonName":"Mavzu : Ko'makchilar",
            "description":"Qisqacha : Ko’makchilar vazifasiga ko’ra kelishiklarga o’xshay",
            "level":"Daraja : A2",
            "data":"Ko’makchilar  lug’aviy  ma’nosini  yo’qotgan  bo’lib,  boshqa  so’zlar  bilan birikib kelgandagina turli munosabatlarni ifodalaydi. Boshqarish xususiyatiga ko’ra sof ko’makchilar ikki guruhga bo’linadi: 1) bir xil kelishikdagi so’z bilan keluvchi sof ko’makchilar, 2) turli kelishiklar bilan qo’llanuvchi sof ko’makchilar.Sayin,  uzra,  orqali  va  boshqa  ayrim  sof  ko’makchilar bosh  kelishikdagi  otlar bilan qo’llanadi: kun sayin, borgan sayin kabi. Bunda ular harakatning takrorlanishi, davom  etishi  kabima’nolarni  ifodalaydi.  M-n:  Ro’zg’ori  ham  kun  sayin  but bo’lyapti. (O.)Orqali  ko’makchisi  vosita  ma’nosini  anglatadi:  Xat  pochta  orqali  olindi. Tufayli ko‘makchisi sabab ma’nosini anglatadi: Sen tufayli ulug‘ martabali bo’ldi. Yanglig‘, kabi  ko’makchilari chog‘ishtirish, o’xshatish kabi ma’nolarni bildiradi: Irodaning  qilich  kabi  o’tkirdir  dami.  (O.)    Sari  o’rin,  payt  va  yo’nalish  kabi ma’nolarni ifodalaydi. Ulg‘aygan sari, yaqinlashgan sari kabilar.Turli  kelishikdagi  otlar  bilan  keluvchi  sof  ko‘makchilar  bosh,  qaratqich, jo’nalishkelishigidagi  so’zlar  bilan  qo’llanadi.    Bunday  ko’makchilarga    kabi, singari, bilan, uchun, qadar  ko’makchilari misoldir.Kabi,  singari  ko’makchilari chog‘ishtirish, o’xshatish  ma’nolarini anglatadi. Bahor toshqini kabi extiroslar bilan keldi. (S.An.) Uning o’zi so’zi singari sof edi. Bilan  ko’makchisi  vosita,  sabab,  maqsad,  payt  kabi  ma’nolarni  anglatadi.  Uning birlan, birla, -ila,  -la kabi shakllari she’riyatda qo’llanadi. Gap bilan va’da berma, ish bilan ber. Tilak bilan kelgan, tong bilan uyg’ongan kabi.Uchun ko’makchisi sabab, maqsad, atalganlik kabi ma’nolarni anglatadi: Bu boyliklar sen uchun, qizim. Ataylab ko‘rish uchun keldi kabi.Qadar  ko’makchisi  chog’ishtirish,  masofa,  vaqt  va  o’ringa  munosabat ma’nolarini anglatadi: Aziza o‘rtoqlarini kechga qadar kutdi.                 Vazifadoshko’makchilar.Vazifadosh ko‘makchilar turli  so’z  turkumlaridan  bo’lib,  ular  gapda  o’rni bilan  ko’makchi  vazifasida  qo’llanadi.  Turkumga  va  ma’no  xususiyatiga  ko’ra funktsional ko’makchilar quyidagi turlarga bo’linadi:           Ot ko’makchilar: tomon, old, yon, orqa, xususida, o’rnida, uchida  kabilar.2. Sifat ko‘makchilar: boshqa, qarshi, tashqari  kabilar.3. Fe’l ko’makchilar: ko’ra, qarab, qaraganda, bo’ylab, boshlab, tortib  kabi.4. Ravish komakchilar: burun, avval, ilgari, bari, so’ng, keyin kabilarKo‘makchi-bog‘lovchi vazifasini ham bajaradigan ko‘makchilar: bilan, dеb, dеya.  Masalan:  daftar  bilan  qalam;  Umrim  shirin  o‘tsin  dеb(dеya),  u  ko‘p mеhnat qildi",
            "lang":"uzb"
        },
        {
            "id":7,
            "lessonName":"Mavzu : Bog'lovchilar",
            "description":"Qisqacha : Gap va gap boʻlaklarini bir-biriga bogʻlaydi. ",
            "level":"Daraja : A2",
            "data":"Bogʻlovchi (grammatikada) — yordamchi soʻz. Gap va gap boʻlaklarini bir-biriga bogʻlaydi. Bogʻlovchi ishlatilishiga koʻra, yakka Bogʻlovchilar („va“, „ammo“, „lekin“, „agar“, „chunki“, „uchun“ va boshqalar), takrorlanuvchi UYUSHIQ BÒLAKLAR („baʼzi“, „bir“, „goh“, „yo“ va boshqalar) ga boʻlinadi. Bogʻlovchi grammatik maʼno va vazifasiga koʻra, teng bogʻlovchi va ergashtiruvchi bogʻlovchilarga ajratiladi. Teng bogʻlovchilar „va“, „hamda“ — biriktiruv bogʻlovchilarga („bahor keldi va daraxtlar uygʻondi“); „ammo“, „lekin“, „birok“, „balki“ — zidlovchi bogʻlovchilarga („u ishga bordi, biroq ishlamadi“); „yo“, „yoki“, „yoxud“, „goh... goh“, „dam... dam“ — ayiruvchi bogʻlovchilarga („dam havo ochiladi, dam yomgʻir yogʻadi“); „na... na“ — inkor bogʻlovchilarga („na yomgʻir yogʻadi, na havo ochiladi“) boʻlinadi. Biriktiruvchi bogʻlovchining vazifasini „bilan“ koʻmakchisi, „ham“, „da“, „u“ („yu“) yuklamalari ham bajarishi mumkin. „U“ („yu“) yuklamasi zidlovchi bogʻlovchi vazifasida ham keladi. Ergashtiruvchi bogʻlovchi koʻpincha bosh gapga qoʻshilib, izohlash va aniqlash mazmunini beruvchi ergash gapni bogʻlab keladi. Ular — „ki (m)“ — aniqlov bogʻlovchi („deydilarkim, oyda ham dogʻ bor“); „chunki“, „shuning uchun“, „negaki“, „toki“ — sabab va maqsad bogʻlovchi („keldi, shuning uchun quvondim“); „goʻyo“, „goʻyoki“ — chogʻishtiruv Bogʻlovchi („u tik agʻanadi, goʻyo ostidan kesilganday boʻldi“); „agar“, „basharti“, „garchi“ — shart va toʻsiqsizlik bogʻlovchi („agar kelsa, gaplashaman“)larga farqlanadi.",
            "lang":"uzb"
        },
        {
            "id":8,
            "lessonName":"Mavzu : Undov so'zlar",
            "description":"Qisqacha : his-hayajon, buyruqxitob, haydash, chaqirish kabi maʼnolarni bildiruvchi..",
            "level":"Daraja : A2",
            "data":"Undovlar — hishayajon, buyruqxitob, haydash, chaqirish kabi maʼnolarni bildiruvchi, ran boʻlaklari bilan grammatik bogʻlanmaydigan soʻzlar turkumi va shu turkumga oid muayyan soʻzlar. U. asosiy xususiyatlariga koʻra 2 turga boʻlinadi: emotsional (hissiy) U. va buyruqxitob (imperativ) U.Emotsional U. kishilarning histuygʻusi, ruhiy holatini bildiruvchi U. boʻlib, ular maʼno jihatidan rang-barangdir. Ijobiy emotsional U. matn va ohang bilan bogʻliq ravishda: 1) shafqat, mehribonlik (iye, o, eh); 2) sevinch, zavqlanish, havas (voy, ho, oʻhhoʻ); 3) qoyil qolish, rohatlanish (hayhay, voy, o); 4) mamnuniyat, faxrlanish, xayrixohlik (eha, eh, ehe, oʻhhoʻ, haya, oʻ); 5) undash, ogohlantirish, tasdiq (hayhay, ha) maʼnolarini bildiradi. Salbiy histuygʻularni ifodalovchi U. esa: 1) eʼtiroz, norozilik, shikoyat, zorlanish (qa, ho, voyey, voy, oh, uh, obbo); 2) jirkanish, nafrat, gʻazab, jahl (e, eh, voy, he, ha); 3) achinish, afsuslanish (he, eh, oh); 4) kinoya, kesatiq, piching, koyish (obbo, oha, oʻhhoʻ, hoo, hayhay, hoyhoy); 5) tanbeh, ogohlantirish, kutilmaganlik (e, hayhay, iya); 6) gʻumgʻussa, qaygʻuoʻkinch (eha, oh, eh, voyey, eh); 7) choʻchish, vahima, dahshat (voyey, oh, o, voydod); 8) ajablanish, hayratlanish, ranjish (i, ee, baybaybay, he) kabi maʼnolarni bildiradi.Tilda rasmodatlar bilan bogʻliq U. ham mavjud boʻlib, ular umumiy holahvol soʻrashish, salomlashish, xayrlashish, ragʻbatlantirish, tashakkur, minnatdorchilik kabi maʼnolarni ifodalaydi. Maye: esonmisiz, omonmisiz, xrrmang, bor boʻling; assalomu alaykum, assalom, salom, xayr, xoʻsh, xoʻp; balli, barakalla, ofarin, qoyil, rahmat, tashakKUR, Qulluq, marhamat va sh.k.Buyruqxitob (imperativ) U. kishilarga nisbatan ularning diqqatini tortish, ogoxlantirish yoki jonivorlarni biror ishni bajarish yoki bajarmaslikka undash kabi maʼnolarni ifodalash uchun xizmat qiladi: odamlarga qaratilgan U.ning ayrimlari (hoy, hey, ey, allo) kishilar diqqatini soʻzlovchiga kdratish uchun, boshqalari esa (tss, qani, jim, tek) taʼkid, buyurish maʼnolarini ifodalash maqsadida qoʻllanadi; hayvon va jonivorlarga qaratilgan U. ularni toʻxtatish, chaqirish, haydash, tinchitish, biror ishni bajarishga undash maqsadlarida qoʻllanadi (behbeh, pishpish, tutu, kuchkuch, mahmah; dirr, xoʻsh, taq, ish; chu, pisht, kisht, xix, kishkish va sh.k.). Oʻzbek tilida emotsional U. tarkibiga ham, buyruqxitob U. tarkibiga ham kiritib boʻlmaydigan alohida U. guruhi ham mavjud. Koʻrsatish (hu, huv, hovv), javob, taʼkidni (hahaha, ha, xoʻshxoʻsh, a, labbay) bildiruvchi U. shu jumladandir.",
            "lang":"uzb"
        },
        {
            "id":9,
            "lessonName":"Mavzu : Taqlid so'zlar",
            "description":"Qisqacha : maʼno va shakliy xususiyatlariga koʻra, alohida guruhni tashkil qiluvchi..",
            "level":"Daraja : A2",
            "data":"Taqlid soʻzlar — maʼno va shakliy xususiyatlariga koʻra, alohida guruhni tashkil qiluvchi, kishilar, hayvonlar va boshqa jonli, jonsiz narsalarning tovushini, harakat-holat obrazini taqlidan ifodalovchi soʻzlar. Taqlid soʻzlarning baʼzilari tovushga taqlidni bildiradi, baʼzilari esa harakat-holat va hodisa obraziga taqlidni koʻrsatadi. Shunga koʻra taqlid soʻzlarning 2 turi farqlanadi: tovushga taqlidni bildiradigan soʻzlar (shuningdek onomatopeya deb ham ataladi); obraz (tasvir)ga taqlidni bildiradigan soʻzlar. Tovushga taqlid soʻzlar eshitish bilan, obrazga taqlid soʻzlar koʻrish bilan aloqadordir, yaʼni tovushga taqlid soʻzlar ifodalagan tovushni faqat eshitish mumkin (qars-qurs, taq, duk-duk, gʻiyq, miyov kabi); obrazga taqlid soʻzlar ifodalagan holat, harakatni faqat koʻrish mumkin (jivir-jivir, lip-lip, yalt-yult, hang-mang kabi).Taqlid soʻzlarga illa,Ulla, ira, la qoʻshimchalari qoʻshilganda feʼl yasaladi. Masalan:Lovulla, taqilla, shaqilla, dupurla, hiqilla, yarqira, sharqira. Va ot bilan sifat ham yasash mumkin:sharshara, qahqaha, jizza, qarsak, varrak, tarsaki bularda ot yasalgan.'Jizzaki'da esa sifat yasalgan. Asosi „jiz“ taqlid soʻzi.",
            "lang":"uzb"
        },
        {
            "id":10,
            "lessonName":"Mavzu : Modal so'zlar",
            "description":"Qisqacha : Soʻzlovchining oʻz fikriga turlicha munosabatini anglatadigan va fikrning aniqligi..",
            "level":"Daraja : A2",
            "data":"Modal soʻzlar (lotincha: modalis — „oʻlchov“, „usul“) — soʻzlovchining oʻz fikriga turlicha munosabatini anglatadigan va fikrning aniqligi, rostligi, gumonli yoki shartliligini ifodalash uchun xizmat qiladigan soʻzlar; shaklan oʻzgarmasligi bilan tavsiflanadi, gap boʻlagi vazifasida kelmaydi, gapning boshqa boʻlaklari bilan sintaktik aloqaga kirishmaydi; butun fikrga yoki uning biror qismiga taalluqli boʻlib, odatda, kirish soʻz boʻlib keladi. Modal soʻzlar 2 katta guruhga boʻlinadi: fikrning aniqligini ifodalovchi modal soʻzlar, bularga rostlik, tasdiq-ishonch, qatʼiylikni bildiruvchi soʻzlar (darhaqiqat, haqiqatan; albatta, shubhasiz, soʻzsiz va shahrik.) kiradi. Fikrning noaniqligini ifodalovchi modal soʻzlar, bularga taxmin, gumon, tusmol kabi maʼnolarni ifodalovchi soʻzlar (shekilli, chogʻi, chamasi; ehtimol, balki, aftidan va boshqalar) kiradi. Modal soʻzlar nutqda pauza, yozuvda vergul bilan ajratiladi. Modal soʻzlar takror qoʻllansa yoki emotsional mazmunli soʻz bilan birga kelsa, maʼno kuchayadi. Soʻzlashuv nutqida birdan optik modal soʻz qoʻllanishi mumkin. Modal soʻzlarni fikrning oʻzaro munosabati (xullas, demak, binobarin, qisqasi, umuman, masalan, chunonchi, jumladan, aksincha, avvalo, avvalambor, nihoyat), eʼtiborni tortish (aytganday, aytmoqchi, zotan), taʼkid (xususan, ayniqsa), emotsional maʼno (koshki, zora, yaxshiyamki, xayriyat, attang, afsuski, ajabo) kabilarni anglatuvchi modal maʼnoli soʻzlardan farqlash kerak. Modal soʻzlar va modal maʼnoli soʻzlar oʻzaro yaqin boʻlib, ularning har biriga xos belgi-xususiyatlarni aniqlash funksional grammatikaning dolzarb vazifalaridandir.",
            "lang":"uzb"
        }
    ]))
  }
  const getName = localStorage.getItem("name");
  const handleChange = (event:React.ChangeEvent<HTMLInputElement>) =>{
      setLanguage(event.target.value as string)
    }
  const startLessonBtn = () => {
    if(language ===""){
     alert("Please Select Language!!!")
    }
    else if(language === "Eng"){
      navigation('layout/english')
    }
    else if(language === "Rus"){
      navigation('layout/russian')
    }
    else if(language === "Uzb"){
      navigation('layout/uzbek')
    }
  }
  return (
    <>
     <Box sx={{maxWidth:"100vw",flexDirection:'column',display:'flex',justifyContent:'center',alignItems:'center'}}>
      <Box sx={{width:"50%",mt:5,flexDirection:"column",display:"flex",justifyContent:'center',alignItems:'center',gap:"20px"}}>
        <Box sx={{background:'#e79822',width:"90%",color:'white',borderRadius:"15px",boxShadow:2}}>
        <Typography variant="h4" sx={{textAlign:'center'}}>{getName} Please Select Language : </Typography>
        </Box>
        <TextField
          label='Selected language'
          select 
          value = {language}
          onChange = {handleChange}
          fullWidth
          color='warning'
        >
          <MenuItem value='Uzb'>Uzbek</MenuItem>
          <MenuItem value='Rus'>Русский язык</MenuItem>
          <MenuItem value='Eng'>English</MenuItem>
        </TextField>
        <Button variant="outlined" color="warning" onClick={startLessonBtn}>Go!!!</Button>
      </Box>
    </Box>
    </>
   
  )
}

export default SelectLanguage