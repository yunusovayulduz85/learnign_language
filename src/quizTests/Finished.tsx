import React  from 'react'
import { useQuiz } from '../context/QuizContext';

const Finished = () => {
  const { point, maxPossiblePoints, dispatch } = useQuiz();
  console.log(maxPossiblePoints);
  console.log(point);
  return (
    <>
      <h1>Finished with {point} / {maxPossiblePoints} </h1>
      <button className='btn btn-ui' onClick={() => dispatch({ type: "Restart" })}>Restart</button>
    </>
  )
}

export default Finished