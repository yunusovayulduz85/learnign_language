import { LessonCardWrapper } from '../../styles/LessonCart.modules'
import useAxios from '../../hooks/useAxios'
import { Skeleton, Typography} from '@mui/material'
import { ItemType } from '../../types/lesson'
import LessonCard from '../../ui/LessonsCard'

const RussionLesson = () => {
  const getRussianLesson = localStorage.getItem("russian")
  const getRussianLessonArr = getRussianLesson? JSON.parse(getRussianLesson):[]
  return (
   <>
    <Typography variant="h3" sx={{textAlign:'center',mt:"-50px",mb:2,color:'#ff9800'}}>
         Все уроки
      </Typography>
   <LessonCardWrapper>
      {
        getRussianLessonArr?.map((item:ItemType , index:number) => {
        return<>
        <LessonCard {...item}/>
        </>
         })
        }
     
     </LessonCardWrapper>
   </>
   
  )
}

export default RussionLesson