import styled from "@emotion/styled";

export const LessonCardWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    gap: 10px;
    justify-content: center;
`
