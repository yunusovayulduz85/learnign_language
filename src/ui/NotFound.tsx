import React from 'react'
import NotFoundGif from "../assets/NotFoundGif.gif"
import { Box, Button } from '@mui/material'
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import { useNavigate } from 'react-router';
const NotFound = () => {
    const navigation = useNavigate()
    const navigateHomePage = () =>{
        navigation('/')
    }
  return (
    <>
     <Box sx={{display:'flex',justifyContent:'center' , mt:0.5, alignItems:'center'}}>
        <img src={NotFoundGif} alt='gif with NotFound Page'/>
    </Box>
    <Box sx={{display:'flex',justifyContent:'center',mt:2}}>
        <Button onClick={navigateHomePage} style={{display:'flex',alignItems:'center',gap:'5px',background:"#ff9800",color:'white',fontSize:'16px',padding:"5px 10px",border:"1px solid",borderRadius:"10px"}}>
        <KeyboardBackspaceIcon/>
        go to home page
    </Button>
    </Box>
    
    </>
   
  )
}

export default NotFound