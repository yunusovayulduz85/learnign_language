import useAxios from '../../hooks/useAxios'
import { ItemType } from '../../types/lesson';
import DetailLessonsCard from '../DetailLessonsCard';

const DetailRussianLesson = () => {
   const currentPath: string = window.location.pathname;
  //current path
  const pathParts: string[] = currentPath.split('/');
  //End of path
  const endOfPath: string = pathParts[pathParts.length - 1];
  const getRussianLesson = localStorage.getItem("russian")
  const getRussianLessonArr = getRussianLesson? JSON.parse(getRussianLesson):[]
  return (
    <>
     {
        getRussianLessonArr?.map((item:ItemType,index:number) => {
            if(item.id === Number(endOfPath)){
               return <DetailLessonsCard {...item} key={index}/>
            }
        })
    }
    </>
  )
}

export default DetailRussianLesson