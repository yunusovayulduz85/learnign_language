import useAxios from '../../hooks/useAxios'
import { ItemType } from '../../types/lesson'
import DetailLessonsCard from '../DetailLessonsCard'

const DetailUzbekLesson = () => {
  const getUzbekLesson = localStorage.getItem("uzbek")
  const getUzbekLessonArr = getUzbekLesson? JSON.parse(getUzbekLesson):[]
  const currentPath: string = window.location.pathname;
  //current path
  const pathParts: string[] = currentPath.split('/');
  //End of path
  const endOfPath: string = pathParts[pathParts.length - 1];
  return (
    <>
       {
        getUzbekLessonArr?.map((item:ItemType,index:number) => {
            if(item.id === Number(endOfPath)){
               return <DetailLessonsCard {...item} key={index}/>
            }
        })
    }
     </>
  )
}

export default DetailUzbekLesson