import { useState } from "react";
import { ItemType } from "../types/lesson";
import { Avatar, Box, Button, Card, CardActions, CardContent, Typography } from "@mui/material";
import { deepOrange } from "@mui/material/colors";
import Progress from "../quizTests/Progress";
import Questions from "../quizTests/Questions";
import NextButton from "../quizTests/NextButton";
import { QuizWrapper } from "../styles/Quiz.modules";
import Timer from "../quizTests/Timer";
import { useNavigate } from "react-router";

const DetailLessonsCard = (item:ItemType) => {
 
 const [startQuiz, setStartQuiz] = useState<boolean>(false)
 const navigation = useNavigate()
  return   <>
             <Box sx={{display:'flex',justifyContent:'center',mt:"15px"}} >
                    <Card sx={{width:"500px",background:'#f3ece1',boxShadow:3}}>
                        <CardContent>
                            <Box sx={{display:'flex',justifyContent:'center',mb:"10px"}}>
                               <Avatar sx={{ bgcolor: deepOrange[500] }}>{item.id}</Avatar>
                            </Box>
                        <Typography variant='h6' sx={{textAlign:'center'}}>
                          {item.lessonName}
                        </Typography>
                        <Typography variant='h4' sx={{textAlign:'center'}}>
                            {item.lang === "eng"&& 'General informations'}
                            {item.lang === "rus" && 'Общая информация'}
                            {item.lang === "uzb" && 'Umumiy Ma\'lumotlar'}
                        </Typography>
                        <Typography variant='body1' sx={{textAlign:'justify'}}>{item.data}</Typography>
                        </CardContent>
                        <CardActions sx={{display:'flex',justifyContent:'center',alignItems:'center'}}>
                            <Button  variant='contained' color='warning' onClick={() => navigation(-1)}>
                                {item.lang === "eng" && "Back"}
                                {item.lang === "rus" && "Назад"}
                                {item.lang === "uzb"&& "Orqaga"}
                            </Button>
                            <Button  variant='contained' color='warning' onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => setStartQuiz(true)}>
                                {item.lang === "eng" && "Start Quiz"}
                                {item.lang === "rus" && "Начать тест"}
                                {item.lang === "uzb"&& "Testni boshlash"}
                            </Button>
                        </CardActions>
                    </Card>
                </Box>
                {/* { */}
                    {/* startQuiz &&  <QuizWrapper>
                    {/* {
                        status === "loading" && <CircularProgress />
                    }
                    {
                        status === "error" && <Error />
                    }
                    {
                        status === "ready" && <StartScreen/>
                    }
                    {
                        status === "active" &&
                        <> */}
                        <Progress />
                        <Questions />
                        <Timer />
                        <NextButton />
                        {/* </> */}
                    {/* // } */}
                    {/* // { */}
                    {/* //     status === "finish" && <Finished /> */}
                    {/* // } */}

                    {/* </QuizWrapper> */} */
                 {/* } */}
               

     </>

  
  
}

export default DetailLessonsCard