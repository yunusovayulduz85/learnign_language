import { useQuiz } from '../context/QuizContext';

function StartScreen() {
    const {questions,dispatch}=useQuiz();
    console.log(questions);
    return (
        <div className='start'> 
            <h1>StartScreen</h1>
            <h1>Welcome to The React Quiz</h1>
            <h3>{questions.length} questions to test your React mastery</h3>
            <button className='btn btn-ui' onClick={()=>dispatch({type:"start"})}>Start Quiz</button>
        </div>
    )
}

export default StartScreen;


