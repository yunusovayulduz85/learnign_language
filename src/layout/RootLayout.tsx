import { Outlet, useNavigate } from 'react-router'
import { userDataWrapper, NavbarWrapper } from '../styles/Header.modules'
import { AppBar, Autocomplete, Box, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Drawer, IconButton, Tab, Tabs, TextField, Toolbar, Typography, linkClasses, makeStyles, useMediaQuery } from '@mui/material'
import { useContext, useState } from 'react'
import LanguageIcon from '@mui/icons-material/Language';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import MenuIcon from '@mui/icons-material/Menu';
import { Link } from 'react-router-dom'
const RootLayout = () => {
  const language = sessionStorage.getItem("language")
  const currentPath: string = window.location.pathname;
  //current path
  const pathParts: string[] = currentPath.split('/');
  //End of path
  const endOfPath: string = pathParts[pathParts.length - 1];
  // console.log( endOfPath);
  const [lessonName, setLessonName] = useState<string | null>(null)
  //*tavfsiyalar modalkasi uchun
  const [open, setOpen] = useState<boolean>(false)
  //*menu barni chiqishi uchun
  const [menuBar, setMenuBar] = useState(false)
  const smallScreen = useMediaQuery('(max-width:850px)')
  //*menu barni ochilib yopilishi uchun
  const [isDrawerOpen, setIsDarwerOpen] = useState(false)
  const lessons = [""]
  const getName = localStorage.getItem("name")
  const navigation = useNavigate()
  const LogOut = () => {
    localStorage.removeItem("name")
    localStorage.removeItem("password")
    navigation('/')
    window.location.reload()
  }
  return (
    <>
        <NavbarWrapper >
            <AppBar sx={{padding:"8px",backgroundColor:"#ff9800"}}>
              <Toolbar sx={{display:'flex',alignItems:'center',justifyContent:'space-between'}}>
                <Box sx={{display:'flex',width:"100%", alignItems:'center',gap:"20px"}}>
                <Box sx={{display:'flex',gap:'5px',alignItems:'center'}}>
                <Box><LanguageIcon/></Box>
                <Typography 
                variant='h6'
                noWrap
                component="a"
                href={currentPath}
                sx={{color:'white',textDecoration:'none',fontWeight: 700,letterSpacing: '.2rem',}}
                >
                     {
                   language === "Eng" || endOfPath ==="english" ?("English"):(
                   language === "Rus" || endOfPath === "russian" ? ("Русский язык"):("Ozbek tili")
                   )
                  }
                </Typography>
                  </Box>
                
                {
                  !smallScreen  ? (
                    <><Typography
                    variant='body1'
                    noWrap
                    component="a"
                    href='/SelectLanguage'
                    sx={{color:'white',textDecoration:'none',fontWeight: 700,}}
                    >
                      {language === "Eng" || endOfPath === "english" ? ("Change Language") : (
                        language === "Rus" || endOfPath === "russian" ? ("Изменить язык") : ("Tilni o'zgartirish")
                      )}
                  </Typography>
                      <Typography variant='body1' onClick={() => setOpen(true)}  style={{color:'white',fontWeight: 700}}>
                      {language === "Eng" || endOfPath === "english" ? ("Recommendation") : (
                        language === "Rus" || endOfPath === "russian" ? ("Рекомендация") : ("Tavfsiyalar")
                      )}
                    </Typography>
                </>
                  ):(<IconButton size='large' edge='start' color='inherit' aria-label='logo' onClick={()=>setIsDarwerOpen(true)}>
                    <MenuIcon />
                    </IconButton>)
                }
                </Box> 
                  <Box sx={{display:'flex',alignItems:'center',gap:'10px'}}>
                     <Box sx={{color:'white',display:'flex',alignItems:'center',gap:'5px'}}>
                      <Typography 
                        variant='h6'
                        noWrap
                        component="a"
                        href='personalData'
                        sx={{color:'white',textDecoration:'none'}}
                      >{getName}</Typography>
                      <AccountCircleIcon/>
                    </Box>
                    <Button onClick={LogOut} variant='contained' color='warning'>
                         {language === "Eng" || endOfPath === "english" ? ("LogOut") : (
                        language === "Rus" || endOfPath === "russian" ? ("Выйти") : ("Chiqish")
                      )}
                    </Button>  
                  </Box>
                    
              </Toolbar>
            </AppBar>
        </NavbarWrapper>

                <Dialog open={open} onClose={() => setOpen(false)} aria-labelledby='dialog-title' aria-description='dialog-description'>
                    <DialogTitle id='dialog-title'>
                        {
                        language === "Eng" || endOfPath ==="english" ?("Tips for learning a language"):(
                        language === "Rus" || endOfPath === "russian" ? ("Советы по изучению языка"):("Til o'rganish bo'yicha maslahatlar")
                        )}

                      </DialogTitle>
                    <DialogContent>
                      <DialogContentText id='dialog-description'>
                        {
                        language === "Eng" || endOfPath ==="english" ?("In the beginning, excellence is demanded in learning any field! While teaching, make an effort to distance yourself from distracting thoughts. Not Remember any other task and direct your focus immediately to the area you are currently engaged in. Also, keep in mind that any success comes as a result of consistent efforts! (Thank you for your attention 😊)"):(
                        language === "Rus" || endOfPath === "russian" ? ("С самого начала требуется отличие в освоении любой области! Во время преподавания старайтесь отдалиться от отвлекающих мыслей. Вспомните о каком-то другом деле и немедленно направьте свое внимание на ту область, в которой вы в данный момент заняты. Также помните, что любой успех приходит в результате последовательных усилий! (Спасибо за ваше внимание 😊)"):("Avvalambor har qanday sohani o'rganishda teranlik talab qilinadi! Dars qilish mobaynida sizni chalg'ituvchi omillardan iloji boricha uzoqlashishga harakat qiling.Har qanday boshqa ishingizni unuting va bor fokusingizni ayni damda shug'ullanayotgan sohangizga qarating. Shuni ham aytib o'tish joizki har qanday muvaffaqqiyat to'xtovsiz harakatlar natijasidir!(E'tiboringiz uchun rahmat😊) ")
                        )}
                      </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                      <Button onClick={() => setOpen(false)}>
                        {
                        language === "Eng" || endOfPath ==="english" ?("Close"):(
                        language === "Rus" || endOfPath === "russian" ? ("закрывать"):("Yopish")
                        )}
                      </Button>
                    </DialogActions>
                </Dialog>
                <Drawer anchor='left' open={isDrawerOpen} onClose={()=>setIsDarwerOpen(false)}>
                <Box p={2} width='250px' textAlign='center' role='presentation'>
                          <Typography variant='h6' component='div'>
                        {
                        language === "Eng" || endOfPath ==="english" ?("Menu"):(
                        language === "Rus" || endOfPath === "russian" ? ("Меню"):("Menu")
                        )}
                          </Typography>
                          <Box sx={{display:'flex',flexDirection:'column',gap:"15px",mt:4}}>
                    <Typography
                    variant='h6'
                    noWrap
                    component = "a"
                    href='/SelectLanguage'
                    sx={{color:'#ff9100',textDecoration:'none'}}
                    >
                      {language === "Eng" || endOfPath === "english" ? ("Select Language") : (
                        language === "Rus" || endOfPath === "russian" ? ("Изменить язык") : ("Tilni o'zgartirish")
                      )}
                  </Typography>
        
                      <Button onClick={() => setOpen(true)} variant='contained' color='warning' sx={{cursor:'pointer'}}>
                      {language === "Eng" || endOfPath === "english" ? ("Recommendation") : (
                        language === "Rus" || endOfPath === "russian" ? ("Рекомендация") : ("Tavfsiyalar")
                      )}
                    </Button>
                  </Box>
               </Box>
                </Drawer>
        <Outlet/>
    </>
        
  )
}

export default RootLayout