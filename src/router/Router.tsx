import { Route, Routes } from "react-router"
import SelectLanguage from "../components/SelectLanguage"
import CreateAccount from "../components/Registration"
// import Login from "../components/Login"
import RootLayout from "../layout/RootLayout"
import EnglishLesson from "../components/lessons/EnglishLesson"
import RussianLesson from '../components/lessons/RussianLesson';
import UzbekLesson from "../components/lessons/UzbekLesson"
import Login from "../components/Login"
import DetailEnglishLesson from "../components/detailLessons/DetailEnglishLesson"
import DetailUzbekLesson from "../components/detailLessons/DetailUzbekLesson"
import DetailRussianLesson from "../components/detailLessons/DetailRussianLesson"
import PersonalData from "../ui/PersonalData"
import Home from "../ui/Home"
import Navbar from "../ui/Navbar"
import NotFound from "../ui/NotFound"



const Router = () => {
  return (
    <>
    <Routes>
      <Route path="/" element={<Navbar/>}>
      <Route index element={<Home/>}/>
      <Route path='registration' element={<CreateAccount/>}/>
      <Route path="Login" element={<Login/>}/>
      <Route path="SelectLanguage" element={<SelectLanguage/>}/>
      <Route path='SelectLanguage/layout/' element={<RootLayout/>}>
        <Route path='english' element={<EnglishLesson/>}/>
        <Route path='russian' element={<RussianLesson/>}/>
        <Route path='uzbek' element={<UzbekLesson/>}/>
        <Route path="english/:id" element={<DetailEnglishLesson/>}/>
        <Route path="russian/:id" element={<DetailRussianLesson/>}/>
        <Route path="uzbek/:id" element={<DetailUzbekLesson/>}/>
      </Route>
      
      <Route path="SelectLanguage/layout/personalData" element={<PersonalData/>}/>
      <Route path="personalData" element={<PersonalData/>}/>
      </Route>
      
      <Route path="*" element={<NotFound/>}/>
      
    </Routes>
    </>
  )
}

export default Router