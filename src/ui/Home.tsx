import { Box, Button, Grid, Typography } from "@mui/material"
import HomeImg from "../assets/home_img.webp"
import { useNavigate } from "react-router"
import EastIcon from '@mui/icons-material/East';
const Home = () => {
  const navigation = useNavigate()
  const SignIn = () =>{
    navigation("Login")
  }
  return (
    <Box sx={{maxWidth:'screen',minHeight:'screen'}}>
       <Grid container spacing={2} sx={{alignItems:'center',justifyContent:'space-between',mt:2}}>
      <Grid item xs={6}>
        <Box sx={{display:'flex',flexDirection:'column'}}>
        <Typography variant="h2" sx={{textAlign:'center',fontWeight:'bold',color:'#dbb051'}}>Learn world languages quickly and easily with us!</Typography>
       <Box sx={{display:'flex',justifyContent:'center',mt:2}}>
        <Button onClick={SignIn} variant="contained" color="warning" sx={{paddingX:6,paddingY:1,display:'flex',alignItems:'center',gap:1}}>
          Login
          <EastIcon/>
          </Button>
       </Box>
        </Box>
      </Grid>
      <Grid item xs={6}>
       <img style={{borderRadius:"30px"}} width={"75%"} src={HomeImg} alt="Learn world languages quickly and easily with us"/>
      </Grid>
    </Grid>
    </Box>
   
  )
}

export default Home