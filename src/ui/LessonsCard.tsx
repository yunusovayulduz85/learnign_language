import { Avatar, Box, Button, Card, CardActions, CardContent, Skeleton, Typography } from "@mui/material"
import { ItemType } from "../types/lesson"
import { Link } from "react-router-dom"
import { deepOrange } from "@mui/material/colors";
import { useState } from "react";
import VerifiedIcon from '@mui/icons-material/Verified';
  let check:boolean=false
  const LessonCard = (lesson:ItemType) => {  
  const [viewedLess, setViewedLess] = useState([])
  const ChangeCheck = (id:number) =>{
    check = true
  }
  return <>
      
        <Card sx={{mt:1,width:"350px",display:'flex',flexDirection:'column',boxShadow:3,background:"#f2e4cf",justifyContent:'space-between'}}>
             <CardContent>
               <Box sx={{display:'flex',justifyContent:'center',mb:"10px"}}>
                    <Avatar sx={{ bgcolor: deepOrange[500] }}>{lesson.id}</Avatar>
                </Box>
               <Typography variant='h5' sx={{textAlign:'center',color:"red"}}>
               {lesson.lessonName} 
                </Typography>
               <Typography variant='body1' sx={{textAlign:'center'}}>
                {lesson.description}
                </Typography>
               <Typography variant='h6' sx={{textAlign:'center',color:"#966721"}}>
                 {lesson.level}
                </Typography>
             </CardContent>
             <CardActions sx={{display:'flex',justifyContent:'center'}}>
              <Link to={`${lesson.id}`}>
              <Button
               variant='contained'
               color='warning'
               onClick={()=>ChangeCheck(lesson.id)}
              >
                 {lesson.lang === "eng" && "Start Lesson"}
                 {lesson.lang === "rus" && "Начать урок"}
                 {lesson.lang === "uzb" && "Darsni boshlash"}
              </Button>
              </Link>
              {/* {
                check && <VerifiedIcon/>                
              } */}

             </CardActions>
         </Card>
  </>
}

export default LessonCard