import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import LanguageIcon from '@mui/icons-material/Language';
import { Link, Outlet } from 'react-router-dom';
const pages = ['Select Language', 'Registrations', 'SignIn'];
const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

function ResponsiveAppBar() {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event:any) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event:any) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  const getName = localStorage.getItem("name")
  return (
    <>
    <AppBar position="static" sx={{backgroundColor:"#ff9800"}}>
      <Container maxWidth="xl">
        <Toolbar disableGutters sx={{display:'flex',alignItems:'center'}}>
          {/* <Link to={'/'} style={{display:'flex',alignItems:'center',color:'white'}}> */}
           <LanguageIcon sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }} />
          <Typography
            variant="h6"
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 2,
              display: { xs: 'none', md: 'flex' },
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color:'#fff',
              textDecoration: 'none',
            }}
          >
            Languages
          </Typography>
          {/* </Link> */}
         

          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              <MenuItem onClick={handleCloseNavMenu} style={{textAlign:'center'}}>
              <Typography
               variant='h6'
               noWrap
               component = "a"
               href='SelectLanguage'
               sx={{color:'#ff9100',textDecoration:'none'}}
               
               >
                Select Language
                </Typography>
              </MenuItem>
              <MenuItem onClick={handleCloseNavMenu} style={{textAlign:'center'}}>
              <Typography 
               variant='h6'
               noWrap
               component = "a"
               href='registration'
               sx={{color:'#ff9100',textDecoration:'none'}}>
                Registrations
                </Typography>
              </MenuItem>
              <MenuItem onClick={handleCloseNavMenu} style={{textAlign:'center'}}>
              <Typography 
               variant='h6'
               noWrap
               component = "a"
               href='Login'
               sx={{color:'#ff9100',textDecoration:'none'}}
              >
                Sign in
                </Typography>
              </MenuItem>
             
            </Menu>
          </Box>
          <LanguageIcon sx={{ display: { xs: 'flex', md: 'none' }, mr: 1,color:'white' }}/>
          <Typography
            variant="h5"
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 2,
              display: { xs: 'flex', md: 'none' },
              flexGrow: 1,
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color:'white',
              textDecoration: 'none',
            }}
          >
            Languages
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            <Button onClick={handleCloseNavMenu} sx={{ my: 2, color: 'white', display: 'block' }}>
              <Typography 
               variant="body1"
               noWrap
               component = "a"
               href='SelectLanguage'
               sx={{color:'white',textDecoration:'none',fontWeight: 700,}}
              >
              Select Language
              </Typography>
            </Button>
            <Button onClick={handleCloseNavMenu} sx={{ my: 2, color: 'white', display: 'block' }}>
              <Typography 
                variant="body1"
                noWrap
                component = "a"
                href='registration' 
                sx={{color:'white',textDecoration:'none',fontWeight: 700,}}
              >
                Registrations
              </Typography>
            </Button>
            <Button onClick={handleCloseNavMenu} sx={{ my: 2, color: 'white', display: 'block' }}>
              <Typography 
                variant="body1"
                noWrap
                component = "a"
                href='Login' 
                sx={{color:'white',textDecoration:'none',fontWeight: 700,}}
              >
                Sign in
              </Typography>
            </Button>
          </Box>

          <Box sx={{ flexGrow: 0 }}>
            <Box sx={{display:'flex',alignItems:'center',gap:'5px'}}>
               <Typography
                variant='body1'
                noWrap
                component="a"
                href={"personalData"}
                sx={{color:'white',textDecoration:'none',fontWeight: 700,letterSpacing: '.2rem',}}
               >{getName}</Typography>
                  <IconButton href={"personalData"} onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                    <Avatar alt="User" src="" />
                  </IconButton>
            </Box>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
    <Outlet/>
    </>
    
  );
}
export default ResponsiveAppBar;
