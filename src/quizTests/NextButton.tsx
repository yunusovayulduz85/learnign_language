import React from 'react'
import { useQuiz } from '../context/QuizContext'

function NextButton() {
  const { answer, dispatch, index, numberOfQuestions } = useQuiz()
  if (answer == null) {
    return null
  } else if (index + 1 == numberOfQuestions) {
    return <button
      className='btn btn-ui'
      onClick={() => dispatch({ type: "finished" })}>
      Finish
    </button>
  }
  else {
    return (<button
      className='btn btn-ui'
      onClick={() => dispatch({ type: "nextQuestions" })}>
      Next
    </button>)
  }
}

export default NextButton