import React, { useContext } from 'react'
import { useQuiz } from '../context/QuizContext'

const Progress = () => {
    const { index, point, numberOfQuestions, maxPossiblePoints } = useQuiz()
    return (
        <div className=''>
            <progress max={numberOfQuestions} value={index} />
            <p>Questions <strong>{index + 1}</strong> / {numberOfQuestions} </p>
            <p><strong>{point} / {maxPossiblePoints}</strong></p>
        </div>
    )
}

export default Progress


