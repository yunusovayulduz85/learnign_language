
import { useParams } from 'react-router-dom'
import useAxios from '../../hooks/useAxios';
import { ItemType } from '../../types/lesson';
import { Box, Button, Card, CardActions, CardContent, Typography } from '@mui/material';
import DetailLessonsCard from '../DetailLessonsCard';

const DetailEnglishLesson = () => {
  const currentPath: string = window.location.pathname;
  //current path
  const pathParts: string[] = currentPath.split('/');
  //End of path
  const endOfPath: string = pathParts[pathParts.length - 1];
  const getEnglishLesson = localStorage.getItem("english")
  const getEnglishLessonArr = getEnglishLesson? JSON.parse(getEnglishLesson):[]
  
  return (
    <>
    {
        getEnglishLessonArr?.map((item:ItemType,index:number) => {
            if(item.id === Number(endOfPath)){
               return <DetailLessonsCard {...item} key={index}/>
            }
        })
    }
    </>
  )
}

export default DetailEnglishLesson