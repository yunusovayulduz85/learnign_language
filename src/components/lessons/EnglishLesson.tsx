import React from 'react'
import useAxios from '../../hooks/useAxios'
import { Skeleton, Typography} from '@mui/material'
import LessonCard from '../../ui/LessonsCard'
import { LessonCardWrapper } from '../../styles/LessonCart.modules'
import { ItemType } from '../../types/lesson'
const EnglishLesson:React.FC = () => {
  const getEnglishLesson = localStorage.getItem("english")
  const getEnglishLessonArr = getEnglishLesson? JSON.parse(getEnglishLesson):[]
  return <>
     <Typography variant="h3" sx={{textAlign:'center',mt:"-50px",mb:2,color:'#ff9800'}}>
         All Lessons
      </Typography>
  <LessonCardWrapper>
    
        {
        getEnglishLessonArr?.map((item:ItemType , index:number) => {
        return<>
        <LessonCard {...item} key={index} />
        </>
         })
        }
      </LessonCardWrapper>   
  </>
   
}

export default EnglishLesson