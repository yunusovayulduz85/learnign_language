import { Alert, AlertProps, Box, Button, Grid, Snackbar, Stack, TextField, Typography } from "@mui/material"
import { Link, useNavigate } from "react-router-dom"
import LoginImage from "../assets/loginImage.avif"
import { orange } from "@mui/material/colors"
import React, { forwardRef, useContext, useState } from "react"
import { loginItemType } from "../types/user"

const Login = () => {
    const navigation = useNavigate()
    const [name, setName] = useState<string>("")  
    const [password, setPassword] = useState<number>()
    const backClick = () => {
        navigation('/')
    }
    const getName : string | null = localStorage.getItem("name")
    const getPassword : string | null = localStorage.getItem("password")
    const getPasswordNum : number | null = getPassword !== null ? Number(getPassword) : null
    const proceedLogin = () => {
        if(validate()){
            if(getName === name && getPasswordNum === password){
              navigation('/SelectLanguage')
              alert("Successfully!")
              window.location.reload()
            }else{
              alert('Error in entering password or username!')
            }
    }}

    const validate = () => {
        let result = true;
        if(name === "" || name === null){
            result = false;
            alert("Please Enter the name")
        }
        if(!password){
            result = false;
            alert("Please Enter the password")
        }
        return result;
    }
  return (
    <Box sx={{display:'flex',justifyContent:'center',alignItems:'center',mt:22, gap:"0px"}}>
      <Grid container rowSpacing={0} sx={{width:"1000px",bgcolor:'white',paddingX:'40px',paddingY:"30px",borderRadius:"15px",boxShadow:2,justifyContent:'center',alignItems:'center'}}>
        <Grid item xs={5}>
         <img width={"90%"} style={{borderTopLeftRadius:'15px',borderBottomLeftRadius:'15px',display:'flex',justifyContent:'end'}} src={LoginImage} alt="learning_language"/>
        </Grid>
            <Grid item xs={7}>
               <Stack spacing={2}>
                    <Typography variant="h4" color={orange[500]}>User Login</Typography>
                    <TextField  label='Name..' variant='outlined' color="warning" value={name} onChange={( e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => setName(e.target.value)}/>
                    <TextField label='Password..' type="password" variant='outlined' color="warning" value={password}  onChange={(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => setPassword(Number(e.target.value))}/>
                      <Box sx={{display:"flex",justifyContent:"end",gap:'5px'}}>
                         <Button onClick={backClick}  variant='contained' color='warning'>Back</Button>
                         <Button onClick={proceedLogin}  variant='contained' color='warning'>LogIn</Button>
                       </Box>
                 </Stack>
             </Grid>     
      </Grid>
       
      
    </Box>
  )

}
export default  Login