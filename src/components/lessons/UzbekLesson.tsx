import React from 'react'
import { LessonCardWrapper } from '../../styles/LessonCart.modules'
import { Button, Card, CardActions, CardContent, Skeleton, Typography } from '@mui/material'
import { Link } from 'react-router-dom'
import useAxios from '../../hooks/useAxios'
import { ItemType } from '../../types/lesson'
import LessonCard from '../../ui/LessonsCard'

const UzbekLesson = () => {
  const getUzbekLesson = localStorage.getItem("uzbek")
  const getUzbekLessonArr = getUzbekLesson? JSON.parse(getUzbekLesson):[]
  return (
    <>
     <Typography variant="h3" sx={{textAlign:'center',mt:"-50px",mb:2,color:'#ff9800'}}>
         Barcha Darslar
      </Typography>
    <LessonCardWrapper>
      {
       getUzbekLessonArr?.map((item:ItemType , index:number) => {
        return<>
        <LessonCard {...item}/>
        </>
         })
        }
     </LessonCardWrapper>
    </>
     
  )
}

export default UzbekLesson