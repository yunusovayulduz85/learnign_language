import './App.css';
import { useState } from 'react';
import Router from './router/Router';

function App() {
  const [language, setLanguage] = useState<string>("")
 
  return (
    <Router/>   
  );
}

export default App;
